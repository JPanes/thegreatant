<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Comment;
use Faker\Generator as Faker;

$factory->define(Comment::class, function (Faker $faker) {
    return [
        //
        'comment_content' => $faker->paragraph($nbsentences = 2),
        'follower_id' => App\Follower::inRandomOrder()->first()->id,
        'post_id' => App\Post::inRandomOrder()->first()->id,
    ];
});
