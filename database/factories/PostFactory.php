<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Post;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) 
{
    return [
        //
        'title' => $faker->sentence($nbwords = 3),
        'brief'=> $faker->paragraph($nbsentences = 2),
        'image' => $faker->imageUrl($width = 500, $height = 400,"business"),
        'post_content' => $faker->paragraph($nbSentences = 60, $variableNbSentences = true),
        'follower_id' => App\Follower::inRandomOrder()->first()->id,
    ];
});
