<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Follower;
use Faker\Generator as Faker;

$factory->define(Follower::class, function (Faker $faker) 
{
    return [
        //
        'first_name' => $faker->firstName(),
        'last_name' => $faker->lastName(),
        //Set a 0.05% chance for rank to be Zealot
        'rank' => $faker->optional($weight = 0.95, $default ="Zealot")->jobTitle(),
        'picture' => $faker->imageUrl($width = 225, $height = 300,"people"),
        'year_joined' => $faker->year($max= "now"),
        'sect_id' => App\Sect::inRandomOrder()->first()->id,
    ];
});
