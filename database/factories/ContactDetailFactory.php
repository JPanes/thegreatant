<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ContactDetail;
use Faker\Generator as Faker;

$factory->define(ContactDetail::class, function (Faker $faker) 
{
    return [
        //
        'country' => $faker->country(),
        'city' => $faker->city(),
        'address' => $faker->address(),
        'postcode' => $faker->postcode(),
        'phone_number' => $faker->e164PhoneNumber(),
        
        //Create a new follower for each contact detail
        'follower_id' => function() 
        {
            return factory(\App\Follower::class)->create()->id;
        }
    ];
});
