<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Sect;
use Faker\Generator as Faker;

$factory->define(Sect::class, function (Faker $faker) 
{
    return [
        //
        'name' => $faker->company(),
        'country_based' => $faker->country(),
        'motto' => $faker->catchPhrase(),
        'year_founded'=> $faker->year(),
    ];
});
