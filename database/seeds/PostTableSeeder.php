<?php

use Illuminate\Database\Seeder;
use App\Post;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Create some test data
        $p1 = new Post;
        $p1->title = "Our Beginning";
        $p1->brief = "For by the Great Ant's guidance, I shall bestow upon you the knowledge of its majesty.";
        $p1->image = "https://dailym.ai/36IWu4B";
        $p1->post_content = "Hail brethren and sistern,
        For by the Great Ant's guidance, I shall bestow upon you the knowledge of its majesty.
        For from the waste of the ether the Great Ant arrived, like us, it too faced hardship and toil within these new lands. However, by its wisdom, it forged the first Colony, Wrieford, and brought about the time of peace for our people.
        Wrieford a glorious place, where its tunnels dig deep into the earth, spreading throughout a vast area and providing ample space for its followers to thrive.
        But alas, the Great Ant was not alone, the False Gods of man disdained by the Followers of the Great Ant, grew jealous of its wonder and prosperity.
        Thus, they conspired to drive out ant-kind and slay the Great Ant.
        For this, they poured all of their anger and cruelty, their hatred and malice into a foul creation. From this, the greatest threat to ant-kind emerged. It would go by many names in many tongues, and serve as a dark story told to larvae warning of what resides within the dark.
        The Termite, a terrible abomination, where ever the ant went, they followed in their hordes. In turn, the eternal war began.
        For on a million lands within a million worlds, this struggle is felt by all of ant-kind. Wrieford stood firm in the face of adversity, blessed by The Great Ant, guarding her.  Diligently maintaining the pheromones laid down millennia ago by the first ants, so that in life and death, all ants may find their way.
        From their toil, all ants may find their way there.
        From their toil, Wrieford is a bastion of The Great Ant and its people, for its mighty entrance hill stands and the tunnels delve deeper by the day.
        
        But, this is the tail of one world. Where, by the Blessed feelers of the Great Ant, its followers spread in search of more. So that under the watchful gaze of its beautiful majesty, more may grow.
        But all ants work towards the blessed vision of tomorrow, where ant-kind prosper in peace. But, for now, their purpose is to see the worlds be once more clean of the Termite scourge.
        \n
        Praise be to the Great Ant";
        $p1->follower_id = "1";
        $p1->save();
        $p1->contributions()->attach(2);
        $p1->contributions()->attach(3);

        $p2 = new Post;
        $p2->title = "The Termite Scourge";
        $p2->brief = "For by the Great Ant's guidance, I shall bestow upon you the knowledge of our foe.";
        $p2->image = "https://3.imimg.com/data3/WU/EC/MY-13010112/anti-termite-treatment-9953944007-250x250.jpeg";
        $p2->post_content = "Hail brethren and sistern,

        For by the Great Ant's guidance, I shall bestow upon you the knowledge of the Termite scourge.
        They the Termites are bad, Great Ant good, let its holy pheromones guide you";
        $p2->follower_id = "2";
        $p2->save();
        $p2->contributions()->attach(1);

        //Generate a large amount of fake posts
        factory(App\Post::class, 100)->create();

        $followers = App\Follower::all();

        //Connect posts to followers
        App\Post::all()->each(function ($posts) use ($followers) 
        {
            $posts->contributions()->attach(
                $followers->random(rand(1,8))->pluck('id')->toArray()
            );
        });
        
    }
}
