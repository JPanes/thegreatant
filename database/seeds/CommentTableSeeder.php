<?php

use Illuminate\Database\Seeder;
use App\Comment;

class CommentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Create sine test data
        $c1 = new Comment;
        $c1->comment_content = "Beep boop, I am a comment";
        $c1->follower_id = "1";
        $c1->post_id = "2";
        $c1->save();

        $c2 = new Comment;
        $c2->comment_content = "Beep boop, I am a comment no2";
        $c2->follower_id = "2";
        $c2->post_id = "1";
        $c2->save();

        factory(\App\Comment::class, 500)->create();
    }
}
