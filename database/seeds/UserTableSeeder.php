<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Faker\Generator as Faker;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $u1 = new User;
        $u1->name = "MouthOfTheGreatOne";
        $u1->email = "www.joepanes@thegreatant.org";
        $u1->password = Hash::make("beepboop");
        $u1->follower_id = "1";
        $u1->is_admin= true;
        $u1->save();

        $u2 = new User;
        $u2->name = "ThatOtherGuy";
        $u2->email = "www.poejanes@thegreatant.org";
        $u2->password = Hash::make("boopbeep");
        $u2->follower_id = "2";
        $u2->save();

        factory(App\User::class, 249)->create();
    }
}
