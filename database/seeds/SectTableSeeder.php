<?php

use Illuminate\Database\Seeder;
use App\Sect;

class SectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $s1 = new Sect;
        $s1->name = "The Blessed of Wrieford";
        $s1->country_based = "Wrieford";
        $s1->motto = "A good termite is a dead one";
        $s1->year_founded = "41";
        $s1->save();

        $s2 = new Sect;
        $s2->name = "Guardians of Sanctum Sanctorum";
        $s2->country_based = "Wrieford";
        $s2->motto = "May the pheromones of the Great Ant guide us true";
        $s2->year_founded = "234";
        $s2->save();

        $s3 = new Sect;
        $s3->name = "The Hellantic Guard";
        $s3->country_based = "Greece";
        $s3->motto = "One vision! One purpose! As pain is to death!";
        $s3->year_founded = "-435";
        $s3->save();

        factory(App\Sect::class, 7)->create();
    }
}
