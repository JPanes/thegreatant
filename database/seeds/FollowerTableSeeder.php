<?php

use Illuminate\Database\Seeder;
use App\Follower;
class FollowerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $f1 = new Follower;
        $f1->first_name = "Joe";
        $f1->last_name = "Panes";
        $f1->rank = "High Priest of the Great Ant";
        $f1->picture = "https://i.imgur.com/fYsdtrW.png";
        $f1->year_joined = "1812";
        $f1->sect_id ="3";
        $f1->save();

        $f2 = new Follower;
        $f2->first_name = "Poe";
        $f2->last_name = "Janes";
        $f2->rank = "Zealot";
        $f2->picture = "https://i.imgur.com/sSpa7db.png";
        $f2->year_joined = "1562";
        $f2->sect_id ="3";
        $f2->save();
    }
}
