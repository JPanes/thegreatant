<?php

use Illuminate\Database\Seeder;
use App\ContactDetail;
class ContactDetailTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $cd1 = new ContactDetail;
        $cd1->country = "United Kingdom";
        $cd1->city = "Swansea";
        $cd1->address = "2 Ohhimark Str.";
        $cd1->postcode = "SA1 8EP";
        $cd1->phone_number = "01632 960165";
        $cd1->follower_id= "1";
        $cd1->save();

        $cd1 = new ContactDetail;
        $cd1->country = "Germany";
        $cd1->city = "Haßmersheim";
        $cd1->address = "Burg Hornberg 3";
        $cd1->postcode = "74865 Neckarzimmern";
        $cd1->phone_number = "01632 960165";
        $cd1->follower_id= "2";
        $cd1->save();

        factory(\App\ContactDetail::class, 249)->create();
    }
}

