<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_details', function (Blueprint $table) 
        {
            $table->bigIncrements('id');
            $table->string('country');
            $table->string('city');
            $table->string('address');
            $table->string('postcode');
            $table->string('phone_number')->nullable();
            $table->bigInteger('follower_id')->unsigned()->unique();
            $table->timestamps();
            
            
            $table->foreign('follower_id')->references('id')->on('followers')
                ->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_details');
    }
}
