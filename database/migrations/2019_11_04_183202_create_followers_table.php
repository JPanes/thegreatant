<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFollowersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('followers', function (Blueprint $table) 
        {
            $table->bigIncrements('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('rank');
            $table->string('picture');
            $table->integer('year_joined');
            $table->bigInteger('sect_id')->unsigned();
            $table->timestamps();
            
            
            $table->foreign('sect_id')->references('id')->on('sects')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('followers');
    }
}
