<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sect extends Model
{
    protected $fillable = [
        'name',
        'motto',
        'country_based',
        'year_founded'];
        
    //One-to-many relationship with the sects model
    public function followers()
    {
        return $this->hasMany('App\Follower');
    }
}
