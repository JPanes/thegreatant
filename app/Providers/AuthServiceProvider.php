<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('update-post' ,'App\Policies\PostPolicy@update');
        Gate::define('delete-post' ,'App\Policies\PostPolicy@delete');

        Gate::define('update-follower' ,'App\Policies\FollowerPolicy@update');
        Gate::define('delete-follower' ,'App\Policies\FollowerPolicy@delete');

        Gate::define('update-comment' ,'App\Policies\CommentPolicy@update');
        Gate::define('delete-comment' ,'App\Policies\CommentPolicy@delete');

        Gate::define('create-sect' ,'App\Policies\SectPolicy@create');
        Gate::define('update-sect' ,'App\Policies\SectPolicy@update');
        Gate::define('delete-sect' ,'App\Policies\SectPolicy@delete');

        //
    }
}
