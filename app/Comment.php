<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
        'comment_content'
    ];

    //One-to-many relationship with follower model
    public function followers()
    {
        return $this->belongsTo('App\Follower','follower_id');
       
    }

    //One-to-many relationship with post model
    public function posts()
    {
        return $this->belongsTo('App\Post','post_id');
    }

}
