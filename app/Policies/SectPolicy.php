<?php

namespace App\Policies;

use App\Sect;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SectPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any sects.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the sect.
     *
     * @param  \App\User  $user
     * @param  \App\Sect  $sect
     * @return mixed
     */
    public function view(User $user, Sect $sect)
    {
        //
    }

    /**
     * Determine whether the user can create sects.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
        return $user->is_admin;
    }

    /**
     * Determine whether the user can update the sect.
     *
     * @param  \App\User  $user
     * @param  \App\Sect  $sect
     * @return mixed
     */
    public function update(User $user, Sect $sect)
    {
        //
        return $user->is_admin;
    }

    /**
     * Determine whether the user can delete the sect.
     *
     * @param  \App\User  $user
     * @param  \App\Sect  $sect
     * @return mixed
     */
    public function delete(User $user, Sect $sect)
    {
        //
        return $user->is_admin;
    }

    /**
     * Determine whether the user can restore the sect.
     *
     * @param  \App\User  $user
     * @param  \App\Sect  $sect
     * @return mixed
     */
    public function restore(User $user, Sect $sect)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the sect.
     *
     * @param  \App\User  $user
     * @param  \App\Sect  $sect
     * @return mixed
     */
    public function forceDelete(User $user, Sect $sect)
    {
        //
    }
}
