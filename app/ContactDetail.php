<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactDetail extends Model
{
    protected $fillable = [ 
        'country',
        'city', 
        'address', 
        'postcode', 
        'phone_number',
        'follower_id'];
        
    //One-to-one relationship with the followers model
    public function followers()
    {
        return $this->belongsTo('App\Follower','follower_id');
    }
}
