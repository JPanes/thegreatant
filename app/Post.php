<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{   
    protected $fillable = [
        'title',
        'brief',
        'image',
        'post_content'];
    
    //One-to-many relationship with followers model
    public function authors()
    {
        return $this->belongsTo('App\Follower','follower_id');
    }
    
        //Many-to-many relationship with followers model
    public function contributions()
    {
        return $this->belongsToMany('App\Follower');
    }

    //One-to-many relationship with the comments model
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    
}
