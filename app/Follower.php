<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Follower extends Model
{
    protected $fillable = [
        'first_name', 
        'last_name', 
        'picture', 
        'rank',
        'year_joined', 
        'sect_id'];

    //One-to-many relationship with the followers model
    public function sects()
    {
        return $this->belongsTo('App\Sect','sect_id');
       
    }

    //One-to-one relationship with the contact details model
    public function contact_details()
    {
        return $this->hasOne('App\ContactDetail');
    }

    //One-to-many relationship with the post model
    public function authors()
    {
        return $this->hasMany('App\Post');
    }

    //Many-to-many relationship with the post model
    public function contributions()
    {
        return $this->belongsToMany('App\Post');
    }

    //One-to-many relationship with the comments model
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    //One-to-one relationship with the user model
    public function users()
    {
        return $this->hasOne('App\User');
    }
}
