<?php
namespace App\Repositories;

use App\Picture;

class PictureRepository implements PictureRepositoryInterface
{
    /**
     * Get's a picture by it's ID
     *
     * @param int
     * @return collection
     */
    public function get($picture_id)
    {
        return Picture::find($picture_id);
    }

    /**
     * Get's all posts.
     *
     * @return mixed
     */
    public function all()
    {
        return Picture::all();
    }

    /**
     * Deletes a picture.
     *
     * @param int
     */
    public function delete($picture_id)
    {
        Post::destroy($picture_id);
    }

    /**
     * Updates a post.
     *
     * @param int
     * @param array
     */
    public function update($picture_id, array $picture_data)
    {
        Post::find($picture_id)->update($picture_data);
    }

    /**
     * Creates a post.
     *
     * @param array
     */
    public function create(array $picture_data)
    {
        $p = new Picture;
        $p->md5 = $picture_data['md5'];
        $p->imgur_link = $picture_data['imgur_link'];

        $p->save();
    }

}