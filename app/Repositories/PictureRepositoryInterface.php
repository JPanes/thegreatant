<?php
namespace App\Repositories;

interface PictureRepositoryInterface
{
    /**
     * Get's a picture by it's ID
     *
     * @param int
     */
    public function get($picture_id);

    /**
     * Get's all pictures.
     *
     * @return mixed
     */
    public function all();

    /**
     * Deletes a picture.
     *
     * @param int
     */
    public function delete($picture_id);

    /**
     * Updates a picture.
     *
     * @param int
     * @param array
     */
    public function update($picture_id, array $picture_data);

    
    public function create(array $picture_data);
}