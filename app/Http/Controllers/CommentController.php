<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Post;
use App\Comment;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $post = Post::findOrFail($id);
     
        return view('comments.create', ['post' => $post]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id, $user_id)
    {
        //
        $validatedData = $request->validate([
            'comment_content' => 'required|max:750',
        ]);
        
        $c = new Comment;
        $c->comment_content = $validatedData['comment_content'];
        $c->follower_id = $user_id;
        $c->post_id = Post::findOrFail($id)->id;

        $c->save();

        session()->flash('message', 'Comment was created.');

        return redirect()->route('posts.show',['id' => $id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $comment = Comment::findOrFail($id);
        if (Gate::allows('update-comment', $comment)) 
        {
            return view('comments.edit', ['comment' => $comment]);
        }else
        {
            abort(403, 'Access denied');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        if (Gate::allows('update-comment', Comment::find($id))) 
        {
            $validatedData = $request->validate([
                'comment_content' => 'required|max:750',
            ]);

            Comment::findOrFail($id)->update($validatedData);
            $post_id = Comment::findOrFail($id)->post_id;

            session()->flash('message', 'Comment was updated');

            return redirect()->route('posts.show', ['id' => $post_id]);
        }else
        {
            abort(403, 'Access denied');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $comment = Comment::findOrFail($id);
        $post_id = $comment->post_id;
        $comment->delete();

        return redirect()->route('posts.show', ['id' => $post_id])->with('message', 'Comment was deleted');
    }
}
