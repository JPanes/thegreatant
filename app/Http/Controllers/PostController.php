<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\UploadedFile;
use App\Http\Controllers\Controller;
use App\Repositories\PictureRepositoryInterface;
use App\Post;
use App\Follower;
use Imgur;
use Yish\Imgur\Upload;


class PostController extends Controller
{

     /**
     * PictureController constructor.
     *
     * @param PictureRepositoryInterface $post
     */
    public function __construct (PictureRepositoryInterface $picture) {
        $this->picture = $picture;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $posts = Post::paginate(15);
        return view('posts.index',['posts' => $posts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $user_id)
    {
        //
        $validatedData = $request->validate([
            'title' => 'required|max:100',
            'brief' => 'required|max:3000',
            //'image' => 'required|max:400',
            //Prevent from going over max characters
            'post_content'  => 'required|max:4294967295',
        ]);
        $p = new Post;
        $p->title = $validatedData['title'];
        $p->brief = $validatedData['brief'];
        $p->image = "https://i.imgur.com/rN70iFL.png";
        $p->follower_id = $user_id;
        $p->post_content = $validatedData['post_content'];
        
        $p->save();

        //$p->followers()->attach($validatedData['author_id']);

        session()->flash('message', 'Post was created.');

        return redirect()->route('posts.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $post = Post::findOrFail($id);
        
        return view('posts.show', ['post' => $post]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {  
        $post = Post::findOrFail($id);
        
        if (Gate::allows('update-post', $post)) 
        {
            return view('posts.edit', ['post' => $post]);
        }else 
        {
            abort(403, 'Access denied');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit_image($id)
    {
        $post = Post::findOrFail($id);
        if (Gate::allows('update-post', $post)) 
        {
            return view('posts.edit_image', ['post' => $post]);
        }else 
        {
            abort(403, 'Access denied');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        if (Gate::allows('update-post', Post::find($id))) 
        {
            $validatedData = $request->validate([
                'title' => 'required|max:100',
                'brief' => 'required|max:3000',
                'image' => 'required|max:400',
                //Prevent from going over max characters
                'post_content'  => 'required|max:4294967295',
            ]);
            
            Post::findOrFail($id)->update($validatedData);
            
            session()->flash('message', 'Post was updated.');

            return redirect()->route('posts.show', ['id' => $id]);
        }else
        {
            abort(403, 'Access denied');
        }
    }

    /**
     * Update the specified resource in storage post image.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_image(Request $request, $id)
    {
        if (Gate::allows('update-post', Post::find($id))) 
        {   
            $validatedImage = $request->validate([
                'image' => 'required | image'
            ]);
            //Generate an md5 output of the file
            $md5_input_picture = md5_file($request->image);
            
            //Find out whether this image has potentially already been uploaded before
            foreach($this->picture->all() as $stored_image)
            {
                //Compare md5s current and previously submitted
                if($md5_input_picture == $stored_image->md5)
                {
                    $validatedPicture['image'] = $stored_image->imgur_link;
                    Post::findOrFail($id)->update($validatedPicture);

                    session()->flash('message', 'Post was updated.');

                    return redirect()->route('posts.show', ['id' => $id]);
                }
                
            }
            
            //If it can't find a the image

            //Upload to Imgur using the API
            $imgur_image = Imgur::upload($request->image);

            //Add the image data to the database
            $picture_data = array (
                'md5' => $md5_input_picture,
                'imgur_link' => $imgur_image->link(),
            );
            
            $this->picture->create($picture_data);

            $validatedData['image'] = $picture_data['imgur_link'];
            Post::findOrFail($id)->update($validatedData);

            session()->flash('message', 'Post was updated.');

            return redirect()->route('posts.show', ['id' => $id]);

        }else 
        {
            abort(403, 'Access denied');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $post = Post::findOrFail($id);
        if (Gate::allows('delete-post', $post)) 
        {
            session()->flash('status', 'Task was successful!');
            $post->delete();
            return redirect()->route('posts.index')->with('message', 'Post was deleted');
        } else {
            abort(403, 'Access denied');
        }
        
    }
}
