<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\UploadedFile;
use App\Http\Controllers\Controller;
use App\Repositories\PictureRepositoryInterface;
use App\Follower;
use App\ContactDetail;
use App\Sect;
use Imgur;
use Yish\Imgur\Upload;



class FollowerController extends Controller
{
     /**
     * PictureController constructor.
     *
     * @param PictureRepositoryInterface $post
     */
    public function __construct (PictureRepositoryInterface $picture) {
        $this->picture = $picture;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $followers = Follower::paginate(30);
        return view('followers.index',['followers' => $followers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $sects = Sect::orderBy('name','asc')->get();
        return view('followers.create', ['sects' => $sects]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validatedData = $request->validate([
            'first_name' => 'required|max:123',
            'last_name' => 'required|max:123',
            'picture' => 'required',
            'rank' => 'required|max:100',
            'year_joined' => 'nullable|numeric',
            'sect_id' => 'required|numeric',
            'email' =>  'required|email:rfc,dns',
            'country' => 'required|max:100',
            'city' => 'required|max:100',
            'address' => 'required|max:400',
            'postcode' => 'required|max:50',
            'phone_number' => 'required|max:30',
            'password' =>  'required|string|min:8|confirmed'
        ]);

        $f = new Follower;
        $f->first_name = $validatedData['first_name'];
        $f->last_name = $validatedData['last_name'];
        $f->picture = $validatedData['picture'];
        $f->rank = $validatedData['rank'];
        $f->year_joined = $validatedData['year_joined'];
        $f->sect_id = $validatedData['sect_id'];
        
        $f->save();

        $c = new ContactDetail;
        $c->email = $validatedData['email'];
        $c->country = $validatedData['country'];
        $c->city = $validatedData['city'];
        $c->address = $validatedData['address'];
        $c->postcode = $validatedData['postcode'];
        $c->phone_number = $validatedData['phone_number'];
        $c->follower_id = $f->id;
        $c->save();

        $u = new User;
        $u->password = Hash::make($validatedData['password']);
        $u->follower_id = $f->id;

        $u->save();

        session()->flash('message', 'Follower was created.');

        return redirect()->route('followers.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $follower = Follower::findOrFail($id);
        
        return view('followers.show', ['follower' => $follower]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $follower = Follower::findOrFail($id);
        if (Gate::allows('update-follower', $follower)) 
        {
            $sects = Sect::orderBy('name','asc')->get();
            
            return view('followers.edit', ['follower' => $follower], ['sects' => $sects]);
        }else 
        {
            abort(403, 'Access denied');
        }
    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit_picture($id)
    {
        $follower = Follower::findOrFail($id);
        if (Gate::allows('update-follower', $follower)) 
        {
            return view('followers.edit_picture', ['follower' => $follower]);
        }else 
        {
            abort(403, 'Access denied');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Gate::allows('update-follower', Follower::find($id))) 
        {    
            $validatedFollowerData = $request->validate([
                'first_name' => 'required|max:123',
                'last_name' => 'required|max:123',
                'rank' => 'required|max:100',
                'year_joined' => 'nullable|numeric',
                'sect_id' => 'required|numeric'
            ]);
            $validatedContactData = $request->validate([
                'country' => 'required|max:100',
                'city' => 'required|max:100',
                'address' => 'required|max:400',
                'postcode' => 'required|max:50',
                'phone_number' => 'required|max:30'
            ]);

            Follower::findOrFail($id)->update($validatedFollowerData);
            Follower::findOrFail($id)->contact_details->update($validatedContactData);
            
            session()->flash('message', 'Follower was updated.');

            return redirect()->route('followers.show', ['id'=> $id]);
        }else 
        {
            abort(403, 'Access denied');
        }
    }

     /**
     * Update the specified resource in storage follower image.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_picture(Request $request, $id)
    {
        if (Gate::allows('update-follower', Follower::find($id))) 
        {   
            $validatedPicture = $request->validate([
                'picture' => 'required|image'
            ]);
            //Generate an md5 output of the file
            $md5_input_picture = md5_file($request->picture);
            
            //Find out whether this image has potentially already been uploaded before
            foreach($this->picture->all() as $stored_image)
            {
                //Compare md5s current and previously submitted
                if($md5_input_picture == $stored_image->md5)
                {
                    $validatedPicture['picture'] = $stored_image->imgur_link;
                    Follower::findOrFail($id)->update($validatedPicture);

                    session()->flash('message', 'Follower was updated.');

                    return redirect()->route('followers.index');
                }
                
            }
            
            $imgur_image = Imgur::upload($request->picture);

            $picture_data = array (
                'md5' => $md5_input_picture,
                'imgur_link' => $imgur_image->link(),
            );
            

            $validatedPicture['picture'] = $picture_data['imgur_link'];

            Follower::findOrFail($id)->update($validatedPicture);

            session()->flash('message', 'Follower was updated.');

            return redirect()->route('followers.index');

        }else 
        {
            abort(403, 'Access denied');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $follower = Follower::findOrFail($id);
        if (Gate::allows('delete-follower', Follower::find($id))) 
        {  
            $follower->delete();

            return redirect()->route('followers.index')->with('message', 'Follower was deleted');
        }else 
        {
            abort(403, 'Access denied');
        }
    }
}
