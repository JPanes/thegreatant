<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use App\Follower;
use App\ContactDetail;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            //User account information
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            //Follower information
            'first_name' => ['required', 'max:123'],
            'last_name' => ['required','max:123'],
            'rank' => ['required','max:100'],
            'year_joined' => ['nullable','numeric'],
            'sect_id' => ['required', 'numeric'],
            //Contact details
            'country' => ['required','max:100'],
            'city' => ['required', 'max:100'],
            'address' => ['required','max:400'],
            'postcode' => ['required','max:50'],
            'phone_number' => ['required','max:30']
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        $id = Follower::create([
            'first_name' => $data['first_name'],
            'last_name'=>$data['last_name'], 
            'picture' => "https://i.imgur.com/rN70iFL.png",
            'rank' => $data['rank'],
            'year_joined' => $data['year_joined'],
            'sect_id' => $data['sect_id'],
            ])->id;

        ContactDetail::create([
            'country' => $data['country'],
            'city' => $data['city'],
            'address' => $data['address'],
            'postcode' => $data['postcode'],
            'phone_number' => $data['phone_number'],
            'follower_id' => $id,
        ]);
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'follower_id' => $id,
        ]);
    }
}
