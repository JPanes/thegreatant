<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Sect;

class SectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //
        $sects = Sect::paginate(10);
        return view('sects.index',['sects' => $sects]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::allows('create-sect'))
        {
            return view('sects.create');
        }else
        {
            abort(403, 'Access denied');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Gate::allows('create-sect'))
        {
            $validatedData = $request->validate([
                'name' => 'required|max:100',
                'motto' => 'required|max:255',
                'country_based' => 'required|max:100',
                'year_founded'  => 'required|numeric',
            ]);

            $s = new Sect;
            $s->name = $validatedData['name'];
            $s->motto = $validatedData['motto'];
            $s->country_based = $validatedData['country_based'];
            $s->year_founded = $validatedData['year_founded'];

            $s->save();

            session()->flash('message', 'Sect was created.');

            return redirect()->route('sects.index');
        }else
        {
            abort(403, 'Access denied');
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $sect = Sect::findOrFail($id);
        
        return view('sects.show', ['sect' => $sect]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $sect = Sect::findOrFail($id);

        if (Gate::allows('update-sect', $sect))
        {
            return view('sects.edit', ['sect' => $sect]);
        }else
        {
            abort(403, 'Access denied');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        if (Gate::allows('update-sect', Sect::find($id))) 
        {
            $validatedData =  $request->validate([
                'name' => 'required|max:100',
                'motto' => 'required|max:255',
                'country_based' => 'required|max:100',
                'year_founded'  => 'required|numeric',
            ]);
            
            Sect::findOrFail($id)->update($validatedData);

            session()->flash('message', 'Sect was updated.');

            return redirect()->route('sects.show', ['id' => $id]);

        }else
        {
            abort(403, 'Access denied');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $sect = Sect::findOrFail($id);

        if (Gate::allows('delete-sect', $sect)) 
        {
            $sect->delete();

            return redirect()->route('sects.index')->with('message', 'Sect was deleted');
        }else
        {
            abort(403, 'Access denied');
        }
    }
}
