Hello,

I am currently writing this on 4/4/2021, due to Laravel being a bit of a faff to setup I will share a link to the .mp4 that I gave as evidence for the functionality of the Website.

I remember quite enjoying this module, and was excited to produce a website that would look nice (unlike the travesty that I made in College).

The colour scheme is perhaps a bit plain (since I did not change it from the default for what I used), I personally quite like it.

Why did I choose to base my website around an ant cult?
I found it funny, and since the theme was up to me, may as well go with something memorable.

Also the video is a bit quick, since I had a lot to evidence and only 3 minutes of time allowed.
I also made it a bit jokey since I wanted to try and make it fun for the marker, rather than something that may be a bit boring (since they would be watching several hours of other people's video, may as well make it fun).
https://1drv.ms/v/s!AhkRkEgkv3LxgrYC6Rx9H6yTVJbQYQ

Thanks,
Joe Panes
