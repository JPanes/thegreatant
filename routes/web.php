<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//Followers
Route::get('followers', 'FollowerController@index')->name('followers.index');

//Route::get('followers/create', 'FollowerController@create')->name('followers.create')->middleware('auth');

//Route::post('followers', 'FollowerController@store')->name('followers.store')->middleware('auth');

Route::get('followers/{id}', 'FollowerController@show')->name('followers.show')->middleware('auth');

Route::patch('followers/{id}/update', 'FollowerController@update')->name('followers.update')->middleware('auth');

Route::get('followers/{id}/edit', 'FollowerController@edit')->name('followers.edit')->middleware('auth');

Route::patch('followers/{id}/update/picture', 'FollowerController@update_picture')->name('followers.update_picture')->middleware('auth');

Route::get('followers/{id}/edit/picture', 'FollowerController@edit_picture')->name('followers.edit_picture')->middleware('auth');

Route::delete('followers/{id}', 'FollowerController@destroy')->name('followers.destroy')->middleware('auth');

//Sects
Route::get('sects', 'SectController@index')->name('sects.index');

Route::get('sects/create', 'SectController@create')->name('sects.create')->middleware('auth');

Route::post('sects', 'SectController@store')->name('sects.store')->middleware('auth');

Route::get('sects/{id}', 'SectController@show')->name('sects.show');

Route::patch('sects/{id}/update', 'SectController@update')->name('sects.update')->middleware('auth');

Route::get('sects/{id}/edit', 'SectController@edit')->name('sects.edit')->middleware('auth');

Route::delete('sects/{id}', 'SectController@destroy')->name('sects.destroy')->middleware('auth');

//Posts
Route::get('posts', 'PostController@index')->name('posts.index');

Route::get('posts/create', 'PostController@create')->name('posts.create')->middleware('auth');

Route::post('posts/{user_id}', 'PostController@store')->name('posts.store')->middleware('auth');

Route::get('posts/{id}', 'PostController@show')->name('posts.show');

Route::patch('posts/{id}/update', 'PostController@update')->name('posts.update')->middleware('auth');

Route::get('posts/{id}/edit', 'PostController@edit')->name('posts.edit')->middleware('auth');

Route::patch('post/{id}/update/image', 'PostController@update_image')->name('posts.update_image')->middleware('auth');

Route::get('posts/{id}/edit/image', 'PostController@edit_image')->name('posts.edit_image')->middleware('auth');

Route::delete('posts/{id}', 'PostController@destroy')->name('posts.destroy')->middleware('auth');

//Comments
Route::get('posts/{id}/comments', 'CommentController@create')->name('comments.create')->middleware('auth');

Route::post('posts/{id}/comments/create/{user_id}', 'CommentController@store')->name('comments.store')->middleware('auth');

Route::patch('posts/comments/{id}/update', 'CommentController@update')->name('comments.update')->middleware('auth');

Route::get('posts/comments/{id}/edit', 'CommentController@edit')->name('comments.edit')->middleware('auth');

Route::delete('comments/{id}', 'CommentController@destroy')->name('comments.destroy')->middleware('auth');

//Authentication
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
