@extends('layouts.app')

@section('title','Posts')

@section('content')
<div class="row justify-content-center">
    <div class="card">
        <article class="card-body">
            <div class="container-center">    
                <h3> Posts by followers guided by its mighty pheromones: </h3>
                    @foreach ($posts as $post)
                        <div class="card">
                            <div class="card-body">
                                <h4><a href="{{ route('posts.show', ['id' => $post->id]) }}"> {{ $post->title}}</a></h4>
                                <p>Brief: {{$post->brief}}</p>
                            </div>
                        </div>
                    @endforeach
                <div class="card">
                    <div class="card-footer">
                        <nav class="row justify-content-between">
                            {{ $posts->links() }}

                            <form method="GET" action="{{ route('posts.create') }}">
                                    @csrf
                                    <button type="submit" class="btn btn-primary">Create Post</button>
                            </form>
                        </nav>
                    </div>
                </div>
            </div>
        </article>
    </div>
</div>
@endsection