@extends('layouts.app')

@section('title', 'Create Post')

@section('content')
<div class="row justify-content-center">
    <div class="container">
        <div class="card">
            <article class="card-body">
                <form method="POST" action="{{ route('posts.store', ['user_id' => Auth::id()]) }}" id="postform">
                    @csrf

                    <div class="form-group row">
                        <label for="title" class="col-form-label text-md-right">{{ __('Title:') }}</label>
            
                        <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title') }}" required autocomplete="title" autofocus>
        
                        @error('title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group row">
                        <label for="brief" class="col-form-label text-md-right">{{ __('Brief:') }}</label>
            
                        <input id="brief" type="text" class="form-control @error('brief') is-invalid @enderror" name="brief" value="{{ old('brief') }}" required autocomplete="brief" autofocus>
        
                        @error('brief')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    
                    <div class="form-group row">
                        <label for="post_content" class="col-form-label text-md-right">{{ __('Post Content:') }}</label>
            
                        <textarea id="post_content" class="form-control @error('post_content') is-invalid @enderror" name="post_content" form="postform" cols="75" value="{{ old('post_content') }}" required autocomplete="post_content" autofocus>
                            {{ old('post_content') }}
                        </textarea>
                        @error('post_content')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="card">
                        <div class="card-footer">
                                <nav class="row justify-content-between">
                                    <input type="submit" class="btn-primary" value="Submit">
                                    
                                        @csrf
                                        <a href="{{ route('posts.index') }}"> <button type="submit" class="btn btn-secondary">Cancel</button></a>
                                </nav>
                        </div>
                    </div>
                </form>
            </article>
        </div>
    </div>
</div>
@endsection