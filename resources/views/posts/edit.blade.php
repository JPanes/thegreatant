@extends('layouts.app')

@section('title', 'Edit Post')

@section('content')
<div class="row justify-content-center">
    <div class="container">
        <div class="card">
            <article class="card-body">
                <form method="POST" action="{{ route('posts.update', $post->id) }}" id="postform">
                    @csrf
                    <input type="hidden" name="_method" value="PATCH">

                    <div class="form-group row">
                        <label for="title" class="col-form-label text-md-right">{{ __('Title:') }}</label>
            
                        <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ $post->title }}" required autocomplete="title" autofocus>
        
                        @error('title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group row">
                        <label for="brief" class="col-form-label text-md-right">{{ __('Brief:') }}</label>
            
                        <input id="brief" type="text" class="form-control @error('brief') is-invalid @enderror" name="brief" value="{{ $post->brief }}" required autocomplete="brief" autofocus>
        
                        @error('brief')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    
                    <div class="form-group row">
                        <label for="image" class="col-form-label text-md-right">{{ __('Image:') }}</label>
            
                        <input id="image" type="text" class="form-control @error('image') is-invalid @enderror" name="image" value="{{ $post->image }}" required autocomplete="image" autofocus>
        
                        @error('image')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group row">
                        <label for="post_content" class="col-form-label text-md-right">{{ __('Post Content:') }}</label>
            
                        <textarea id="post_content" class="form-control @error('post_content') is-invalid @enderror" name="post_content" form="postform" cols="75" value="{{ $post->post_content }}" required autocomplete="post_content" autofocus>
                            {{ $post->post_content}}
                        </textarea>
                        @error('post_content')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    
                    
                    <div class="row justify-content-between">
                        <input type="submit" class="btn btn-primary" value="Submit">
                            
                        <form method="GET" action="{{ route('posts.index') }}">
                                @csrf
                                <button type="button" class="btn btn-secondary">Cancel</button>
                        </form>
                    </div>
                </form>
            </article>
        </div>
    </div>
</div>
@endsection