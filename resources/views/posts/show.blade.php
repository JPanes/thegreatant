@extends('layouts.app')

@section('title', 'Post Contents')

@section('content')
<div class="row justify-content-center">
        <div class="container">
            
    
            <div class="card">
                <article>
                    <div class="card-body"> 
                        <h1 class="row justify-content-center">{{ $post->title }}</h1>
                        
                        <div class="row justify-content-center">
                            <img src="{{asset( $post->image )}}" height="400" width="auto" alt="Thumbnail for {{ $post->title }}">
                        </div>
                        
                        <h4>Brief:</h4>
                        <h5>{{ $post->brief }}</h5>
                      
                        <h4>Author:</h4> 
                        <a href="{{ route('followers.show', ['id' => $post->follower_id])  }}"><h5>{{ $post->authors->users->name }} </h5></a>
                    </div>
                </article>
            </div>
            <div class="card">
                <div class="card-body">
                    <h4>Post:</h4>
                    <p> {{ $post->post_content }} </p>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <article>
                        <h5>Contributor(s) To This Post:</h5>
                        <ul>
                            @foreach ($post->contributions as $contributor)
                                <li><a href="{{ route('followers.show', ['id' => $contributor->id]) }}">
                                        {{ $contributor->users->name}}</a></li>
                            @endforeach
                        </ul>
                    </article>
                </div>
            </div>
            
            <div class="card">
                <div class="card-body">
                    <h4>Comments:</h4>
                    @auth
                        <form method="POST" action="{{ route('comments.store', ['id' => $post->id, 'user_id' => Auth::id()]) }}">
                
                                @csrf
                                <article class="card card-body">
                                    <h5>Add Your Own Comment:</h5>
                                    <label for="comment_content" class="col-form-label text-md-left">{{ __('Comment Content:') }}</label>
                    
                                    <input id="comment_content" type="text" class="form-control @error('comment_content') is-invalid @enderror" name="comment_content" value="{{ old('comment_content') }}" required autocomplete="comment_content">
                    
                                    @error('comment_content')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                        <br>
                                    <div class="row justify-content-center">
                                        <button type="submit" class="btn btn-primary">Post Comment</button>
                                    </div>
                                </article>
                            </form>
                        <br>
                    @endauth
                    @foreach ($post->comments as $comment)
                        <article class="card">
                            <div class="card-body">
                                <a href="{{ route('followers.show', ['id' => $comment->followers->id]) }}">
                                    {{ $comment->followers->users->name }}: </a>
                                <q>{{ $comment->comment_content }}</q>  
                                
                                <div class="blockquote-footer">
                                    <h6>{{ $comment->updated_at }}</h6>
                                </div>
                                @auth  
                                    <div class="row justify-content-left">
                                    @if ( Auth::id()  ===  $comment->followers->id)
                                        <form method="GET" action="{{ route('comments.edit', ['id' => $comment->id]) }}">
                                            @csrf
                                            <button type="submit" class="btn-primary">Edit comment</button>
                                        </form>
                                    @endif
                                    @if ( Auth::id()  ===  $comment->followers->id or App\User::find(Auth::id())->is_admin )
                                        <form method="POST" action="{{ route('comments.destroy', ['id' => $comment->id]) }}">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn-secondary">Delete Comment</button>
                                        </form>
                                    @endif
                                    </div>
                                   
                                @endauth    
                            </div>    
                        </article>
                    @endforeach
                </div>
            </div>

            @auth
                @if ( Auth::id()  ===  $post->follower_id  or App\User::find(Auth::id())->is_admin)
                <div class="card">
                    <div class="card-footer">
                        <nav class="row justify-content-between">
                            <form method="GET" action="{{ route('posts.edit', ['id' => $post->id]) }}">
                                @csrf
                                <button type="submit" class="btn-primary">Update Post</button>
                            </form>
                            
                            <form method="GET" action="{{ route('posts.edit_image', ['id' => $post->id]) }}">
                                @csrf
                                <button type="submit" class="btn btn-primary">Update Post's Image</button>
                            </form>

                            <form method="POST" action="{{ route('posts.destroy', ['id' => $post->id]) }}">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn-secondary">Delete Post</button>
                            </form>
                        </nav>
                    </div>
                </div>
                @endif
            @endauth
                </div>
        </div>
</div>
@endsection