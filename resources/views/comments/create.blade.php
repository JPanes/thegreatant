@extends('layouts.app')

@section('title', 'Create Comment')

@section('content')
<div class="row justify-content-center">
    <div class="container">
        <div class="card">
            <article class="card-body">
                <h4>The Post You Are Commenting On:</h4>
                <p>{{ $post->post_content }}</p>
            </article>
        </div>

        <article class="card card-body">
            <h5>Other Comments On the Post:</h5>
            @foreach ($post->comments as $comment)
                <article class="card">
                    <div class="card-body">
                        <a href="{{ route('followers.show', ['id' => $comment->followers->id]) }}">
                            {{$comment->followers->users->name}} </a> : {{$comment->comment_content}} 
                            <div class="blockquote-footer">
                                    <h6>{{ $comment->updated_at }}</h6>
                            </div>
                    </div>
                </article>
            @endforeach
        </article>

        <form method="POST" action="{{ route('comments.store', ['id' => $post->id, 'user_id' => Auth::id()]) }}">
            
            @csrf
            <article class="card card-body">
                <label for="comment_content" class="col-form-label text-md-left">{{ __('Comment Content:') }}</label>

                <input id="comment_content" type="text" class="form-control @error('comment_content') is-invalid @enderror" name="comment_content" value="{{ old('comment_content') }}" required autocomplete="comment_content" autofocus>

                @error('comment_content')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </article>
        
            <nav class="card card-footer">
                <div class="row justify-content-between">
                    <button type="submit" class="btn btn-primary">Submit</button></a>
                    
                    <a href="{{ route('posts.show', ['id' => $post->id]) }}"> <button type="button" class="btn btn-secondary">Cancel</button></a>
                </div>
            </nav>
        </form>
    </div>
</div>
@endsection