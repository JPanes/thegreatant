@extends('layouts.app')

@section('title', 'Create Follower')

@section('content')
    <--**** No longer used, see auth.register for newest version ****-->
    <form method="POST" action="{{ route('followers.store') }}">
        @csrf
        <h3>General Details:</h3>
        <p>First Name: <input type="text" name="first_name"
            value="{{ old('first_name') }}"></p>
        <p>Last Name: <input type="text" name="last_name"
            value="{{ old('last_name') }}"></p>
        <p>Picture: <input type="text" name="picture"
                value="{{ old('picture') }}"></p>
        <p>Rank: <input type="text" name="rank"
            value="{{ old('rank') }}"></p>
        <p>Year Joined: <input type="text" name="year_joined"
            value="{{ old('year_joined') }}"></p>
        <p>Sect: 
            <select name="sect_id">
                @foreach ($sects as $sect)
                <option value="{{  $sect->id  }}"    
                    @if ($sect->id == old('sect_id'))
                        selected="selected"
                    @endif
                > {{ $sect->name  }} </option>
                @endforeach
            </select>    
        </p>

        <h3>Contact Details:</h3>
        <p>Country: <input type="text" name="country"
            value="{{ old('country') }}"></p>
        <p>City: <input type="text" name="city"
                value="{{ old('city') }}"></p>
        <p>Address: <input type="text" name="address"
            value="{{ old('address') }}"></p>
        <p>Postcode: <input type="text" name="postcode"
            value="{{ old('postcode') }}"></p>
        <p>Phone Number: <input type="text" name="phone_number"
            value="{{ old('phone_number') }}"></p>

        <h3>Account Details</h3>
        <p>Email: <input type="email" name="email"
            value="{{ old('email') }}"></p>
        <input type="submit" value="Submit">
        <a href="{{ route('followers.index') }}">Cancel</a>
    </form>
@endsection