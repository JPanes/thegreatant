@extends('layouts.app')

@section('title','Followers')

@section('content')
    <div class="row justify-content-center">
        <article>
            <div class="container-center">
                <h4>The followers of the Great Ant: </h4>
                <ul>
                    @foreach ($followers as $follower)
                        <li><a href="{{ route('followers.show', ['id' => $follower->id]) }}">
                            {{ $follower->users->name }}</a></li>
                    @endforeach
                </ul>
            </div>
        {{ $followers->links() }}
        </article>
    </div>
@endsection