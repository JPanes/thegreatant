@extends('layouts.app')

@section('title','Follower Details')

@section('content')
    <div class="row justify-content-center">
        <div class="container">
                
            <div class="card">
                <div class="card-body">
                    <article>
                        <h3>General Details:</h3>
                        <ul style="list-style-type:none;">
                            <li>First Name: {{ $follower->first_name }}</li>
                            <li>Last Name: {{ $follower->last_name }}</li>
                            <li>Rank: {{ $follower->rank }}</li>
                            <li>Year Joined: {{ $follower->year_joined ?? 'Unknown' }}</li>
                            <li>Sect: <a href="{{ route('sects.show', ['id' => $follower->sects->id]) }}">
                                {{ $follower->sects->name }}</a></li>
                        </ul>
                    </article>
                    <div class="justify-content-center">
                        <img class="img" src="{{ asset($follower->picture) }}" alt="A picture of {{$follower->first_name}} {{$follower->last_name}}" height="300" width="auto">
                    </div>
                </div>
            </div>
            <div class="card">
                <article class="card-body">
                    <h3>Contact Details:</h3>
                    <ul style="list-style-type:none;">
                        <li>Country: {{ $follower->contact_details->country }}</li>
                        <li>City: {{ $follower->contact_details->city }}</li>
                        <li>Address: {{ $follower->contact_details->address }}</li>
                        <li>Post Code: {{ $follower->contact_details->postcode }}</li>
                        <li>Phone Number: {{ $follower->contact_details->phone_number }}</li>
                    </ul>
                </article>
            </div>
            <div class="card">
                <article class="card-body">
                    <h3>Account Details:</h3>
                    <ul style="list-style-type:none;">
                        <li>Username: {{ $follower->users->name }}</li>
                        <li>E-mail: {{ $follower->users->email }}</li>
                    </ul>
                </article>
            </div>
            <div class="card">
                <article class="card-body">
                    <h4>Posts:</h4>
                    <ul>
                        @foreach ($follower->authors as $author)
                            <li><a href="{{ route('posts.show', ['id' => $author->id]) }}">
                                {{ $author->title}}</a>
                            </li>
                        @endforeach
                    </ul>
                </article>
            </div>
            
            <div class="card">
                <article class="card-body">
                    <h4>Contributions:</h4>
                    <ul>
                        @foreach ($follower->contributions as $contribution)
                            <li><a href="{{ route('posts.show', ['id' => $contribution->id]) }}">
                                    {{ $contribution->title}}</a></li>
                        @endforeach
                    </ul>
                </article>
            </div>
            <div class="card">
                <article class="card-body">
                    <h4>Comments:</h4>
                    <ul>
                        @foreach ($follower->comments as $comment)
                            <li>{{$comment->created_at}}, <a href="{{ route('posts.show', ['id' => $comment->posts->id]) }}">
                                {{ $comment->posts->title}}</a>
                        @endforeach
                    </ul>
                </article>
            </div>
            
            @if(Auth::id() === $follower->id or App\User::find(Auth::id())->is_admin)
                <div class="card">
                    <div class="card-footer">
                        <nav class="row justify-content-between">
                            <form method="GET" action="{{ route('followers.edit', ['id' => $follower->id]) }}">
                                @csrf
                                <button type="submit" class="btn btn-primary">Update Follower</button>
                            </form>

                            <form method="GET" action="{{ route('followers.edit_picture', ['id' => $follower->id]) }}">
                                @csrf
                                <button type="submit" class="btn btn-primary">Update Picture</button>
                            </form>

                            <form method="POST" action="{{ route('followers.destroy', ['id' => $follower->id]) }}">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-secondary">Delete Follower</button>
                            </form>
                        </nav>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection