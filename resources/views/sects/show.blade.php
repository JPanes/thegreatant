@extends('layouts.app')

@section('title','Sect Details')

@section('content')
<div class="row justify-content-center">
    <div class="container">
        <div class="card card-body">
            <ul>
                <li>Name: {{$sect->name}}</li>
                <li>Motto: {{$sect->motto}}</li>
                <li>Country Based: {{$sect->country_based}}</li>
                <li>Year Founded: {{$sect->year_founded ?? 'Unknown'}}</li>
                <li>Total Followers: {{count($sect->followers)}}</li>
            </ul>
        </div>
        @auth
            @if (App\User::find(Auth::id())->is_admin) 
            
                <div class="card card-footer">
                    <nav class="row justify-content-between">
                        <form method="GET" action="{{ route('sects.edit', ['id' => $sect->id]) }}">
                            @csrf
                            <button type="submit" class="btn-primary">Update Sect</button>
                        </form>

                        <form method="POST" action="{{ route('sects.destroy', ['id' => $sect->id]) }}">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn-secondary">Delete Sect</button>
                        </form>
                    </nav>
                </div>
            @endif
        @endauth
    </div>
</div>
@endsection