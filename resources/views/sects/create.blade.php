@extends('layouts.app')

@section('title', 'Create Sect')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                                    
                    <form method="POST" action="{{ route('sects.store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="motto" class="col-md-4 col-form-label text-md-right">{{ __('Motto') }}</label>

                            <div class="col-md-6">
                                <input id="motto" type="text" class="form-control @error('motto') is-invalid @enderror" name="motto" value="{{ old('motto') }}" required autocomplete="motto" autofocus>

                                @error('motto')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="country_based" class="col-md-4 col-form-label text-md-right">{{ __('Country Based') }}</label>

                            <div class="col-md-6">
                                <input id="country_based" type="text" class="form-control @error('country_based') is-invalid @enderror" name="country_based" value="{{ old('country_based') }}" required autocomplete="country_based" autofocus>

                                @error('country_based')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="year_founded" class="col-md-4 col-form-label text-md-right">{{ __('Year Founded') }}</label>

                            <div class="col-md-6">
                                <input id="year_founded" type="text" class="form-control @error('year_founded') is-invalid @enderror" name="year_founded" value="{{ old('year_founded') }}" required autocomplete="year_founded" autofocus>

                                @error('year_founded')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        
                        <div class="row justify-content-between">
                            <button type="submit" class="btn btn-primary" value="Submit">Submit</button>
                            
                            <a href="{{ route('sects.index') }}"> <button type="button" class="btn btn-secondary">Cancel</button></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection