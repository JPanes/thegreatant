@extends('layouts.app')

@section('title','Sects')

@section('content')
<div class="row justify-content-center">
    <article>
        <div class="container-center">
            <h4>Sects of the Great Ant:</h4>
            <ul>
                @foreach ($sects as $sect)
                    <li><a href="{{ route('sects.show', ['id' => $sect->id]) }}"> {{ $sect->name}}</a></li>
                @endforeach
            </ul>
        </div> 

        {{ $sects->links() }}
        @auth
            <form method="GET" action="{{ route('sects.create') }}">
                    @csrf
                    <button type="submit" class="btn btn-primary">Create Sect</button>
            </form>
        @endauth
    </article>
</div>
       

@endsection